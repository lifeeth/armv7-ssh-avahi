FROM resin/armv7hf-systemd:jessie 

ENV INITSYSTEM on

RUN apt-get update && apt-get install -y dropbear

ADD init.sh /init.sh

CMD /init.sh
